<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChauffeursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chauffeurs', function (Blueprint $table) {
            
            $table->bigIncrements('id')->index();
            $table->string('nom',50)->nullable();
            $table->string('prenom',50)->nullable();
            $table->index(['nom','prenom']);
            //contacts
            $table->string('telephone1',30)->nullable();
            $table->string('telephone2',30)->nullable();
            $table->string('email')->unique()->nullable();


            //Adresse
            $table->string('adresse',100)->nullable();
            $table->string('permis_conduire')->nullable();
            $table->string('nr_permis_conduire')->nullable();
            $table->enum('type_permis_conduire', ["A", "B",'C',"D","E"])->default("B");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chauffeurs');
    }
}
