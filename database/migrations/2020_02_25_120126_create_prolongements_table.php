<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProlongementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prolongements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date("dbt_prolongement");
            $table->date("fin_prolongement");
            $table->enum('statut_prolongement', ['en cours','terminé'])->default('terminé');
            $table->unsignedBigInteger('location_id');

            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prolongements');
    }
}
