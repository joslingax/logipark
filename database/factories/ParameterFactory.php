<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Option::class, function (Faker $faker) {

    $libelle = "";
    do
    {
        $libelle =  $faker->unique($reset = true)->word;

    }while(\App\TypeMotorisation::whereLibelle($libelle)->first()!=null);
    return [
        "libelle" => $libelle,
        'type_option_id'=>function(){ return factory('App\TypeOption')->create()->id;}


    ];
});

$factory->define(App\TypeMotorisation::class, function (Faker $faker) {

    return [
        "libelle" => $faker->word."-".Str::random(5)

    ];
});

$factory->define(App\StatutPayement::class, function (Faker $faker) {

    return [
        "nature" => $faker->word."-".Str::random(5)

    ];
});
$factory->define(App\TypeDepense::class, function (Faker $faker) {

    return [
        "libelle" => $faker->word."-".Str::random(10)

    ];
});
$factory->define(App\Assureur::class, function (Faker $faker) {

    return [
        "nom" => $faker->word."-".Str::random(5)

    ];
});


$factory->define(App\TypeOption::class, function (Faker $faker) {

    $libelle = "";
    do
    {
        $libelle =  $faker->unique($reset = true)->word;

    }while(\App\TypeOption::whereLibelle($libelle)->first()!=null);
    return [
        "libelle" => $libelle,
        "code" => Str::random(3),
    ];
});

$factory->define(App\Modele::class, function (Faker $faker) {

    $libelle = "";
    do
    {
        $libelle =  $faker->unique($reset = true)->word;

    }while(\App\Modele::whereLibelle($libelle)->first()!=null);

    return [
        "libelle" => $libelle,
        'marque_id' => function(){ return factory('App\Marque')->create()->id;},
    ];
});



$factory->define(App\Pays::class, function (Faker $faker) {

    $libelle = "";
    do
    {
        $libelle =  $faker->unique($reset = true)->word;

    }while(\App\Pays::whereLibelle($libelle)->first()!=null);

    return [
        "libelle" => $libelle

    ];
});

$factory->define(App\TypePermis::class, function (Faker $faker) {


    $libelle = "";
    do
    {
        $libelle =  Str::random(3);

    }while(\App\TypePermis::whereLibelle($libelle)->first()!=null);

    return [
        "libelle" =>$libelle

    ];
});

$factory->define(App\TypeVehicule::class, function (Faker $faker) {
    $libelle = "";
    do
    {
        $libelle = $faker->unique($reset = true)->word;

    }while(\App\TypeVehicule::whereLibelle($libelle)->first()!=null);

    return [
        "libelle" => $libelle

    ];
});

$factory->define(App\Permis::class, function (Faker $faker) {

    $libelle = "";
    do
    {
        $libelle = $faker->unique($reset = true)->word;

    }while(\App\Permis::whereNumero($libelle)->first()!=null);


    return [

        'numero'=> Str::random(14),
        'date_emission'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'type_permis_id'=>function(){ return factory('App\TypePermis')->create()->id;},
        'pays_id'=>function(){ return factory('App\Pays')->create()->id;}
    ];
});

$factory->define(App\Marque::class, function (Faker $faker) {

    $libelle = "";
    do
    {
        $libelle = $faker->unique()->word."".Str::random(4);

    }while(\App\Marque::whereLibelle($libelle)->first()!=null);

    return [

        'libelle'=> $libelle,
        'pays_id'=>function(){ return factory('App\Pays')->create()->id;}
    ];
});


