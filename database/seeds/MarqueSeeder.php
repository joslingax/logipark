<?php

use App\Pays;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class MarqueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $marques = Config::get('cars');
        foreach($marques as $mar)
        {
            $pays =Pays::all()->random();
            $marque=factory('App\Marque')->create(['pays_id'=>$pays->id,'libelle'=> $mar['title']]);
            foreach($mar['models'] as $ms)
            {
                factory('App\Modele')->create(['marque_id'=>$marque->id,'libelle'=>$ms['title'] ]);

            }
        }
    }
}
