import Vue from 'vue'
import Router from 'vue-router'



Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: require('./../pages/Dashboard.vue').default
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: require('./../pages/Dashboard.vue').default
    },

    {
      path: '/utilisateurs',
      name: 'utilisateurs',
      component: require('./../pages/parametres/Utilisateurs.vue').default
    },
    {
      path: '/clients',
      name: 'clients',
      component: require('./../pages/clients/Clients.vue').default

    },
    {
      path: '/voitures',
      name: 'voitures',
      component: require('./../pages/voitures/Voitures.vue').default

    },
    {
      path: '/voitures/ajout',
      name: 'voitures_create',
      component: require('./../pages/voitures/NouvelleVoiture.vue').default

    },
    {
      path: '/voitures/:id',
      name: 'voitures_detail',
      component: require('./../pages/voitures/DetailVoiture.vue').default

    },

    {
      path: '/locations',
      name: 'locations',
      component: require('./../pages/locations/Locations.vue').default

    },
    {
      path: '/locations/ajout',
      name: 'locations_ajout',
      component: require('./../pages/locations/Add.vue').default

    },
    {
      path: '/locations/:id/modification',
      name: 'locations_ajout',
      component: require('./../pages/locations/Add.vue').default

    },
    {
      path: '/locations/:id',
      name: 'ShowLocation',
      component: require('./../pages/locations/Show.vue').default

    },
    {
      path: '/marques',
      name: 'marques',
      component: require('./../pages/parametres/Marques.vue').default

    },
    {
      path: '/options',
      name: 'options',
      component: require('./../pages/parametres/Options.vue').default

    },
    {
      path: '/profil',
      name: 'profil',
      component: require('./../pages/profils/Profil.vue').default

    },
    {
      path: '/roles',
      name: 'roles',
      component: require('./../pages/parametres/roles/Roles.vue').default

    },
    {
      path: '/roles/ajout',
      name: 'roles',
      component: require('./../pages/parametres/roles/Show.vue').default

    },
    {
      path: '/roles/:id',
      name: 'roles_details',
      component: require('./../pages/parametres/roles/Show.vue').default

    },
    {
      path: '/depenses',
      name: 'depenses',
      component: require('./../pages/depenses/Depenses.vue').default

    },
    {
      path: '/flux-finances',
      name: 'flux-finances',
      component: require('./../pages/finances/Fluxfinances.vue').default

    },
    {
      path: '/historiques',
      name: 'historiques',
      component: require('./../pages/parametres/historique/Historiques.vue').default

    },
    {
      path: '/chauffeurs',
      name: 'chauffeurs',
      component: require('./../pages/chauffeurs/All.vue').default

    },
    { path: '/404', name: '404', component: require('./../pages/NotFound.vue').default },
    { path: '*', redirect: '/404' },
  ],
  linkActiveClass: 'active'
})
