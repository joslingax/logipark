<?php

namespace App\Observers;

use App\FluxFinance;
use App\Location;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Log;
use Illuminate\Support\Str;

class LocationObserver
{
    /**
     * Handle the location "created" event.
     *
     * @param  \App\Location  $location
     * @return void
     */
    public function created(Location $location)
    {
        //On recalcule date durée du  de la location
        $nbr_jours = Carbon::parse($location->date_dbt)->diffInDays(Carbon::parse($location->date_fin));
        // on ajoute un numéro unique a la location
        $numero = "LC".Carbon::now()->year."-".$location->id;
        $location->frais_livraison     = $location->livraison_effective ? 5000 : 0;
        $location->numero    = $numero;
        $frais               = $location->livraison_effective ? 5000 : 0  + $location->lavage_carburant;  

        $location->montant   = $nbr_jours*$location->tarif + $frais;
        $location->nbr_jour  = $nbr_jours;

        //on sauvegarde
        $location->save();
        //puis on rafraichit le modele
        $location->load('statutPayement');


        $jours = ($location->nbr_jour > 1) ? "jours":"jour";
        $prefix1 =  ($location->statutPayement->nature=="payé") ? "Paiement total" : (($location->statutPayement->nature=="avance") ? "Avance sur paiement" : "" );
        $prefix2 = $location->parent_id!=null ? " : Prolongement n°".$location->numero : ": Location n° ".$location->numero ;
        $description =$prefix1." ".$prefix2." pour ".$location->nbr_jour." ".$jours." : Du "
                      .Carbon::parse($location->date_dbt)->format('d-m-Y')." au "
                      .Carbon::parse($location->date_fin)->format('d-m-Y');

        
           //on génére un flux financier
          
          if($location->statutPayement->nature=="payé")
          FluxFinance::create(
            [
              "flux"=> $description,
              "montant"=> $location->montant,
              "financiable_id"=> $location->id,
              "vehicule_id"=> $location->vehicule_id,
              "date_transaction"=>Carbon::parse($location->date_dbt)->format('Y-m-d'),
              "financiable_type"=>"App\Location",

            ]);
    }

    /**
     * Handle the location "updated" event.
     *
     * @param  \App\Location  $location
     * @return void
     */
    public function updated(Location $location)
    {
        Log::info("okok".$location->isDirty());
    }

    /**
     * Handle the location "deleted" event.
     *
     * @param  \App\Location  $location
     * @return void
     */
    public function deleted(Location $location)
    {
        //dd($location->id);
        $flux = FluxFinance::where('financiable_id',$location->id)->where('financiable_type','App\Location')->get();
        
        if($flux->isNotEmpty())
        {
            $flux->each(function ($item, $key) {
                
                $item->delete();

            });
        }
    }

    /**
     * Handle the location "restored" event.
     *
     * @param  \App\Location  $location
     * @return void
     */
    public function restored(Location $location)
    {
        //
    }

    /**
     * Handle the location "force deleted" event.
     *
     * @param  \App\Location  $location
     * @return void
     */
    public function forceDeleted(Location $location)
    {
        //
    }

    
}
