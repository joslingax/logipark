<?php

namespace App;

use App\Traits\CanUpload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class Client extends SModel
{
    use CanUpload, LogsActivity;

    protected static $logAttributes = ["date_naissance","nom","prenom","telephone1","telephone2","email","adresse"];
    protected static $logName = 'client';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;
    
      /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $storage_path ="public/documents/identites";
    protected $appends = ['nomComplet','hasFile'];

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé le client <strong>{$this->nomComplet}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé le client <strong>{$this->nomComplet}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié le client <strong>{$this->nomComplet}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié le client <strong>{$this->nomComplet}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté le client <strong>{$this->nomComplet}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté le client <strong>{$this->nomComplet}</strong>";
        }
        
    }

    public function getHasFileAttribute()
    {
        return file_exists(storage_path("/app/public/documents/identites/".$this->piece_identite));
    }
    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->orWhere('email', 'LIKE', "%{$q}%")
                ->orWhere('nom', 'LIKE', "%{$q}%")
                ->orWhere('created_at', 'LIKE', "%{$q}%");
    }

        /**
     * Get the client's full name.
     *
     * @return string
     */
    public function getNomCompletAttribute()
    {
        return ucfirst($this->prenom)." ".ucfirst($this->nom);
    }
}
