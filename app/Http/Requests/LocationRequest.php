<?php

namespace App\Http\Requests;

use App\Rules\LocationDisponible;
use App\StatutPayement;
use Illuminate\Foundation\Http\FormRequest;
use Log;

class LocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'zone_location' =>"required",
            'niveau_carburant' =>"required|in:1/4,1/2,3/4,plein",
            'livraison_effective' =>"required",
            'longue_duree' =>"required",
            'avec_chauffeur' =>"required",
            'tarif' =>"required|numeric",
            
            'nom_chauffeur' => $this->input('avec_chauffeur')==true ? "":"required",

            'date_dbt' =>["required","date",($this->input('date_fin')!="" && $this->input('vehicule_id') !="") 
            ?  new LocationDisponible($this->input('date_dbt'),$this->input('date_fin'),$this->input('vehicule_id'),$this->method()=="PATCH" ? $this->input('id') : null) :''],
            'date_fin' =>['required', 'date',"after_or_equal:date_dbt",
                        function ($attribute, $value, $fail) 
                        {
                            $time1 = explode(" ",$value);
                            $time2 = explode(" ",$this->input('date_dbt'));
                            if ($time1[1] != $time2[1]) 
                            {
                                $fail('Les dates de la location doivent avoir la même heure.');
                            }
                        }
            ],
            'vehicule_id' =>['required',"exists:vehicules,id"],

            'chauffeur_id' =>$this->input('avec_chauffeur')==true ? "required|exists:chauffeurs,id" : "",
            'statut_payement_id' =>["required","exists:statut_payements,id",
             function ($attribute, $value, $fail) 
                {
                    $only = collect(['payé',"reservé","avance"]);
                    $statut= StatutPayement::whereId($value)->first();

                    if (!$only->contains($statut->nature)) 
                    {
                        $fail("Ce statut de payement n'est pas accepté");
                    }
                }
            ],
            'avance' =>$this->input('statut_payement_id')==2 ? "required|numeric|min:0" : "",

            'client_id' =>$this->input('ancien_client')==false ? "required|exists:clients,id" : "",
            'client.nom' =>$this->input('ancien_client')==false ? "" :"required",
            'client.prenom' =>$this->input('ancien_client')==false ?  "": "required",
            'client.adresse' =>$this->input('ancien_client')==false ?  "" :"required",
            'client.telephone1' =>$this->input('ancien_client')==false ?  "" :"required",
            'client.piece_identite' =>$this->input('ancien_client')==false ?  "" :"required",
            'client.type_piece_identite' =>$this->input('ancien_client')==false ?  "" :"required",

            'nbr_jour'=>"required",
        ];
    }


    public function messages()
    {
        return[
            //Step 0
            "date_dbt.required"=>"La date de début est requise",
            "date_dbt.date"=>"Le format de la date est incorrect",
            "date_fin.required"=>"La date de fin est requise",
            "date_fin.date"=>"Le format de la date est incorrect",
            "date_fin.after_or_equal"=>"La date de fin doit dépasser celle de début",
            'nom_chauffeur.required' =>"Le nom du chauffeur est requis",

            'vehicule_id.required' =>"Le vehicule est requis",
            'vehicule_id.exists' =>"Ce vehicule est inconnu",

            'tarif.required' =>"Le tarif est requis",
            'tarif.numeric' =>"Le tarif doit être un nombre",

            
            'statut_payement_id.required' =>"Le statut du payement est requis",
            'statut_payement_id.exists' =>"Ce statut de payement  est inconnu",
            'statut_payement_id.in' =>"Ce statut de payement n'est pas accepté",

            'avance.required' =>"L'avance est requise",
            'avance.numeric' =>"L'avance doit être un nombre",
            'avance.min' =>"L'avance doit être supérieur à 0",

            'client_id.required' =>"Le client est requis",
            'client_id.exists' =>"Ce client est inconnu",

            'chauffeur_id.required' =>"Le chauffeur est requis",
            'chauffeur_id.exists' =>"Ce chauffeur est inconnu",

            "niveau_carburant.required"=>"Le niveau de carburant est requis",
            "niveau_carburant.in"=>"Ce niveau de carburant est inconnu",
            'client.nom.required' =>"Le nom du client est requis",
            'client.prenom.required' =>"Le prénom du client est requis",
            'client.adresse.required' =>"L'adresse du client est requise",
            'client.telephone1.required' =>"Le téléphone du client est requis",
            'client.type_piece_identite.required'=>"Le type de pièce est requis",
            'client.piece_identite.required'=>"Une copie de la pièce est requise",

            'zone_location.required' =>"La zone de location est requise",

        ];
    }
}
