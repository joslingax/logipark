<?php

namespace App\Http\Controllers\Api;

use App\Assureur;
use App\Chauffeur;
use App\Client;
use App\Option;

use App\Marque;
use App\TypePermis;
use App\TypeMotorisation;
use App\TypeVehicule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use App\StatutPayement;
use App\TypeDepense;
use App\TypeOption;
use App\User;
use App\Vehicule;

class OptionDispatcherController extends Controller
{

    public function get(Request $request,  $page)
    {
        //options pour la page vehicule
        if($page =="vehicules")
        {
            return
            [
                "marques"=>Marque::with(['pays','modeles'])->orderBy("marques.libelle",'asc')->get(),
                "motorisations"=>TypeMotorisation::orderBy("libelle",'asc')->get(),
                "tvehicules"=>TypeVehicule::orderBy("libelle",'asc')->get(),
                "tpermis"=>TypePermis::orderBy("libelle",'asc')->get(),
                "voptions"=>TypeOption::with(['options'])->get(),
            ];
        }
        if($page =="vehicule-details")
        {
            return
            [
                "assureurs"=>Assureur::orderBy("assureurs.nom",'asc')->get(),
            ];
        }
        if($page =="utilisateurs")
        {
            return
            [
                "roles"=>Role::orderBy("roles.libelle",'asc')->get(),
            ];
        }
        if($page =="historiques")
        {
            return
            [
                "utilisateurs"=>User::orderBy("name",'asc')->get(),
            ];
        }
        if($page =="role-show")
        {
            return
            [
                "permissions"=>Permission::all()->groupBy('table'),
            ];
        }
        if($page =="locations")
        {
            return
            [
                "vehicules"=>Vehicule::with(['marque',"typeVehicule"])->orderBy("created_at",'desc')->get(),
                "clients"=>Client::orderBy("created_at",'desc')->get(),
                "chauffeurs"=>Chauffeur::orderBy("nom",'asc')->get(),
                "payements"=>StatutPayement::orderBy("nature",'asc')->get()
            ];
        }
        if($page =="depenses")
        {
            return
            [
                "vehicules"=>Vehicule::with(['marque',"typeVehicule"])->orderBy("created_at",'desc')->get(),
                "tdepenses"=>TypeDepense::orderBy("libelle",'desc')->get()
            ];
        }
        if($page =="flux-finances")
        {
            return
                [
                    "vehicules"=>Vehicule::orderBy("libelle",'asc')->get()
                ];
        }
        if($page =="options")
        {
            return
            [
                "toptions"=>TypeOption::orderBy("libelle",'asc')->get()
            ];
        }
    }

}
