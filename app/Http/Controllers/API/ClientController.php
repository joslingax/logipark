<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Client;
use App\Exports\ClientExport;
use App\Http\Requests\ClientRequest;
use Carbon\Carbon;
use DB;
use Log;

class ClientController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        return   Client::search($q)->orderBy("created_at",'desc')->paginate($per);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        try
        {
            DB::beginTransaction();
            $client = Client::create(
                [
                    'nom' =>$request->input('nom'),
                    'prenom' =>$request->input('prenom'),
                    'adresse' =>$request->input('adresse'),
                    'date_naissance' =>$request->input('date_naissance'),
                    'email' =>$request->input('email'),
                    'telephone1' =>$request->input('telephone1'),
                    'type_piece_identite' =>$request->input('type_piece_identite'),
                ]
                );
            
            //on upload le permis

            $file = $request->input('piece_identite');
            $client->upload($file,$client->getMime($file),$client->id);
            

            DB::commit();
            return response()->json(['success' => true],201);

        }catch(\Exception $e)
        {
            DB::rollback();
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        try
        {
            DB::beginTransaction();

            $client->nom =$request->input('nom');
            $client->prenom = $request->input('adresse');
            $client->telephone1 = $request->input('telephone1');
            $client->email = $request->input('email');
            $client->type_piece_identite = $request->input('type_piece_identite');
            $client->date_naissance = $request->input('date_naissance');
            $client->save();

            if($request->input('piece_identite')!=$client->piece_identite)
            {
                //on upload le permis
                $file = $request->input('piece_identite');
                $client->upload($file,$client->getMime($file),$client->id,$client->piece_identite=="default_permis.jpg" ? "" : $client->piece_identite);
            }
            
           
            DB::commit();
            return response()->json(['success' => true],200);

        }catch(\Exception $e)
        {
            DB::rollback();
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
        return response()->json(['success' => true],200);
    }

    public function getFile($id)
    {
        $client = Client::whereId($id)->first();
        $path = storage_path("/app/public/documents/identites/".$client->piece_identite);


        if (file_exists($path)) 
        {
            ob_end_clean();
            return response()->download($path);
        }
        else
        {
            //abort(500, 'Something went wrong');
            return back()->with('status', 'La pièce de ce client est inexistante')
            ->with('type', 'error');
        }
        //return response()->download($path);
    }
    public function export() 
    {

        $q = request()->query('filter') == null ? null : request()->query('filter');
        $format = request()->query('format') == null ? null : request()->query('format');
        $filename = "clients-".Carbon::now()->toDateTimeString();
        if($format)
        {
           if($format=="excel")
           return (new ClientExport($q))->download($filename.'.xlsx');
           if($format=="pdf")
           return Excel::download(new ClientExport($q),$filename.'.pdf');
        }
        
    }
}
