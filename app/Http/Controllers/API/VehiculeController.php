<?php

namespace App\Http\Controllers\API;

use App\Assurance;
use App\CarteGrise;
use App\Exports\VehiculeExport;
use App\Vehicule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\VehiculeRequest;
use App\ViewRecords\VehicleRecord;
use App\VisiteTechnique;
use Carbon\Carbon;
use DB;
use Log;
use Excel;

class VehiculeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 25 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        return   Vehicule::with(['marque','modele',"typeMotorisation","typeVehicule","options"])->search($q)->orderBy("created_at",'desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\VehiculeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VehiculeRequest $request)
    {
        if($request->input('step')==3 || $request->input('step')=="3")
        {

            //ajout des options
            //ajout des images
            //ajout des document

            try
            {

                //on commence la transaction
                DB::beginTransaction();

                    Log::info("On commence la transaction");

                    $date_mise_circulation =Carbon::parse($request->input('date_mise_en_circulation'));
                   //ajout du vehicule
                    $vehicule = Vehicule::create(
                        [
                            //Step 0
                            "libelle"=>$request->input('libelle'),
                            'nbr_porte' =>$request->input('nbr_porte'),
                            'nbr_place' =>$request->input('nbr_place'),
                            'boite_vitesse' => strtolower($request->input('boite_vitesse')),
                            'couleur' =>$request->input('couleur'),
                            'kilometrage' =>$request->input('kilometrage'),
                            'type_motorisation_id' =>$request->input('type_motorisation_id'),
                            'type_vehicule_id' =>$request->input('type_vehicule_id'),
                            'marque_id' =>$request->input('marque_id'),
                            'modele_id' =>$request->input('modele_id'),
                            'type_permis_id'=>null,
                            'tarif'=>$request->input('tarif'),
                            'plaque_immatriculation'=>$request->input('plaque_immatriculation'),
                            'nr_chassis'=>$request->input('nr_chassis'),
                            'date_mise_en_circulation'=>$request->input('date_mise_en_circulation'),
                            'annee_mise_en_circulation'=>$date_mise_circulation->year(),
                            'consommation_au_100_km'=>$request->input('consommation_au_100_km'),

                            //step 1
                            'volume_coffre'=>$request->input('volume_coffre'),
                            'volume_reservoir'=>$request->input('volume_reservoir'),
                            'largeur'=>$request->input('largeur'),
                            'longueur'=>$request->input('slongueur'),
                            'poids'=>$request->input('poids')
                        ]
                    );


                    //On ajoute ses options
                    if(count($request->input('options'))>0)
                    {
                        //En cas de présence d'options, on ajoute
                        $vehicule->options()->attach($request->input('options'));
                    }

                    //Ajout de l'image 
                    if($request->input('image_mise_en_avant')!="" && $request->input('image_mise_en_avant')!=null)
                    {
                        $file = $request->input('image_mise_en_avant');
                        $vehicule->upload($file,$vehicule->getMime($file),$vehicule->id);
                    }
 


                    //ajout de la galerie
                    foreach($request->input('images') as $image)
                    {
                        $file = $image;
                        $vehicule->uploadGallery($file,$vehicule->getMime($file),$vehicule->id);
                    }

                DB::commit();

                return ['success'=>true];
            }
            catch(\Exception $e)
            {
                    DB::rollback();
                    return ['status'=>false,'message'=>$e->getMessage()];

            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicule $vehicule)
    {
         return $this->getVehicule($vehicule->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicule $vehicule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicule $vehicule)
    {
        //base
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function updateBase(Request $request, $id)
    {

        Vehicule::whereId($id)
             ->update([
            "libelle" => $request->input('libelle'),
            "marque_id" => $request->input('marque_id'),
            "modele_id" => $request->input('modele_id'),
            "type_vehicule_id" => $request->input('type_vehicule_id'),
            'boite_vitesse' => strtolower($request->input('boite_vitesse')),
            'type_motorisation_id' => strtolower($request->input('type_motorisation_id')),

            "type_permis_id" => $request->input('type_permis_id'),
            "nbr_place" => $request->input('nbr_place'),
            "nbr_porte" => $request->input('nbr_porte'),
            "couleur" => $request->input('couleur'),
            "tarif" => $request->input('tarif'),
            "kilometrage" => $request->input('kilometrage'),
            "plaque_immatriculation" => $request->input('plaque_immatriculation'),
            "nr_chassis" => $request->input('nr_chassis'),
            "date_mise_en_circulation" => $request->input('date_mise_en_circulation'),
            "consommation_au_100_km" => $request->input('consommation_au_100_km')
        ]);

        return response()->json(['success'=>true,"entity"=>$this->getVehicule($id)],200);

    }


        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function updateDimensions(Request $request, $id)
    {

        Vehicule::whereId($id)
             ->update([
            "volume_reservoir" => $request->input('volume_reservoir'),
            "volume_coffre" => $request->input('volume_coffre'),
            "longueur" => $request->input('longueur'),
            "largeur" => $request->input('largeur'),
            "hauteur" => $request->input('hauteur'),
            "poids" => $request->input('poids'),
        ]);

        return response()->json(['success'=>true,"entity"=>$this->getVehicule($id)],200);
    }

            /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function updateOptions(Request $request, $id)
    {

        $vehicule=Vehicule::whereId($id)->first();

        $vehicule->options()->sync($request->input('options'));

        return response()->json(['success'=>true,"entity"=>$this->getVehicule($id)],200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicule $vehicule)
    {
        //on supprime
        $vehicule->delete();
        return response()->json(['message' => 'Vehicule supprimé avec succès'],200);
    }


    private function getVehicule($id)
    {
        return  Vehicule::with(['marque','modele',"typeMotorisation","carte_grise","images",
        "visite_technique"=>function($query)
        {
            $query->first();
        },
        "typeVehicule","options",'locations.client','assurance.assureur'=> function($query)
        {
            $query->first();
        }])->whereId($id)->first();
        

    }
    public function export() 
    {

        // $q = request()->query('filter') == null ? null : request()->query('filter');
        // $d_fin = request()->query('filter_date_fin') == null ? null : request()->query('filter_date_fin');
        // $d_dbt = request()->query('filter_date_dbt') == null ? null : request()->query('filter_date_dbt');
        // $annee= request()->query('annee') == null ? null : request()->query('annee');
        // $mois = request()->query('mois') == null ? null : request()->query('mois');
        // $vehicule = request()->query('vehicule_id') == null ? null : request()->query('vehicule_id');

        $q = request()->query('q') == null ? null : request()->query('q');
        $format = request()->query('format') == null ? null : request()->query('format');
        $filename = "voitures-".Carbon::now()->toDateTimeString();
        if($format)
        {
           if($format=="excel")
           return (new VehiculeExport($q))->download($filename.'.xlsx');
           if($format=="pdf")
           return Excel::download(new VehiculeExport($q),$filename.'.pdf');
        }
        
    }
}
