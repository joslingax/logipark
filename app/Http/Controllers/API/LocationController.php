<?php

namespace App\Http\Controllers\API;

use App\Client;
use App\Events\CashAdvancedHasBeenMadeEvent;
use App\Exports\LocationExport;
use App\Location;
use App\Http\Controllers\Controller;
use App\Http\Requests\LocationRequest;
use App\StatutPayement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Log;
use Excel;
use Illuminate\Support\Facades\Auth;

class LocationController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per =  request()->query("per_page")  ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $p = request()->query('filter_statut_payement') == null ? null : request()->query('filter_statut_payement');
        $d = request()->query('filter_statut_deroulement') == null ? null : request()->query('filter_statut_deroulement');

        if($per)
        return  Location::with(['vehicule.marque','client',"statutPayement"])->search($q)->deroulement($d)->statut($p)
        ->orderBy("locations.created_at",'desc')->paginate($per);
        else
        return  Location::with(['vehicule.marque',"statutPayement"])->search($q)->deroulement($d)->statut($p)->orderBy("locations.created_at",'desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocationRequest $request)
    {
        try
        {

            //on commence la transaction
            DB::beginTransaction();


            $locationData  = $this->fillData($request);
            //Si le client est ancien
            if($request->input('client_id')==null ||$request->input('client_id')=="" )
            {
                //on cree le nouveau client
                $client= Client::create([
                    "nom"=>$request->input('client.nom'),
                    "prenom"=>$request->input('client.prenom'),
                    "adresse"=>$request->input('client.adresse'),
                    "telephone1"=>$request->input('client.telephone1'),
                    "type_piece_identite"=>$request->input('client.type_piece_identite')
                ]);

                //on actualise les données de la location
                $locationData['client_id'] = $client->id;
                //on upload la piece du client
                $file = $request->input('client.piece_identite');
                $client->upload($file,$client->getMime($file),$client->id,"piece_identite_".$client->id);
            }

               $location =  Location::create($locationData);

               if($request->input("avance")!=null && $request->input("avance")!="")
               {
                event(new CashAdvancedHasBeenMadeEvent($location, $request->input("avance")));  
               }


            DB::commit();
            return response()->json(['success' => true,"entity"=> $location->fresh()->load(['client','vehicule'])],201);

        }
        catch(\Exception $e)
        {
                DB::rollback();
                Log::debug("something bad happened");
                Log::debug($e->getMessage());
                return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {

        return $location->load(['client','vehicule.marque','statutPayement',"chauffeur","fluxfinances"]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(LocationRequest $request, Location $location)
    {
        try
        {

            //on commence la transaction
            DB::beginTransaction();

            $statutPayement = StatutPayement::whereId($request->input('statut_payement_id'))->first();
            $locationData  = $this->fillData($request);

            //Si le client est ancien
            if($request->input('client_id')==null ||$request->input('client_id')=="" )
            {
                //on cree le nouveau client
                $client= Client::create([
                    "nom"=>$request->input('client.nom'),
                    "prenom"=>$request->input('client.prenom'),
                    "adresse"=>$request->input('client.adresse'),
                    "telephone1"=>$request->input('client.telephone1'),
                    "type_piece_identite"=>$request->input('client.type_piece_identite')
                ]);
                 //on actualise les données de la location
                $locationData['client_id'] = $client->id;
                //on upload la piece du client
                $file = $request->input('client.piece_identite');
                $client->upload($file,$client->getMime($file),$client->id,"piece_identite_".$client->id);

            }

            $location->zone_location=$request->input("zone_location");
            $location->niveau_carburant=$request->input("niveau_carburant");
            $location->livraison_effective=$request->input("livraison_effective");
            $location->longue_duree=$request->input("longue_duree");
            $location->avec_chauffeur=$request->input("avec_chauffeur");
            $location->nom_chauffeur=$request->input("nom_chauffeur");
            $location->nom_chauffeur_2=$request->input("nom_chauffeur_2");
            $location->chauffeur_id=$request->input("avec_chauffeur") ? $request->input("chauffeur_id") : null;
            $location->date_dbt=$request->input("date_dbt");
            $location->date_fin=$request->input("date_fin");
            $location->vehicule_id=$request->input("vehicule_id");
            $location->client_id=$locationData['client_id'] ;
            $location->nbr_jour=$request->input("nbr_jour");
            $location->lavage_carburant=$request->input("lavage_carburant");
            $location->frais_livraison=$request->input("frais_livraison");
            $location->montant=$request->input("montant");
            $location->tarif=$request->input("tarif");
            $location->observation=$request->input("observation");

            $location->save();

            DB::commit();
            return response()->json(['success' => true,],200);

        }
        catch(\Exception $e)
        {
                DB::rollback();
                Log::debug($e->getMessage());
                return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $location->delete();
    }

    private function fillData(LocationRequest $request,$hasClient = true)
    {
        return
        [
            'zone_location' =>$request->input("zone_location"),
            'niveau_carburant' =>$request->input("niveau_carburant"),
            'livraison_effective' =>$request->input("livraison_effective"),
            'longue_duree' =>$request->input("longue_duree"),
            'avec_chauffeur' =>$request->input("avec_chauffeur"),
            'nom_chauffeur' =>$request->input("nom_chauffeur"),
            'nom_chauffeur_2' =>$request->input("nom_chauffeur_2"),
            'chauffeur_id' =>$request->input("avec_chauffeur") ? $request->input("chauffeur_id") : null,
            'date_dbt' =>$request->input("date_dbt"),
            'date_fin' =>$request->input("date_fin"),
            'vehicule_id' =>$request->input("vehicule_id"),
            'client_id' =>$request->input("client_id"),
            'nbr_jour'=>$request->input("nbr_jour"),
            'lavage_carburant' =>$request->input("lavage_carburant"),
            'statut_payement_id'=> $request->input("statut_payement_id"),
            'frais_livraison'=>$request->input("frais_livraison"),
            'montant'=>$request->input("montant"),
            'tarif'=>$request->input("tarif"),
            'observation'=>$request->input("observation"),
        ];
    }

    public function export() 
    {

        $q = request()->query('filter') == null ? null : request()->query('filter');
        $payement = request()->query('filter_statut_payement') == null ? null : request()->query('filter_statut_payement');
        $deroulement = request()->query('filter_statut_deroulement') == null ? null : request()->query('filter_statut_deroulement');
        
        $annee= request()->query('annee') == null ? null : request()->query('annee');
        $mois = request()->query('mois') == null ? null : request()->query('mois');
        $vehicule = request()->query('vehicule_id') == null ? null : request()->query('vehicule_id');

        $format = request()->query('format') == null ? null : request()->query('format');
        $filename = "locations-".Carbon::now()->toDateTimeString();
        if($format)
        {
           if($format=="excel")
           return (new LocationExport($q,$deroulement,$payement))->download($filename.'.xlsx');;
        }
        
    }
}
