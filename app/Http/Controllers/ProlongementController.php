<?php

namespace App\Http\Controllers;

use App\Prolongement;
use Illuminate\Http\Request;

class ProlongementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prolongement  $prolongement
     * @return \Illuminate\Http\Response
     */
    public function show(Prolongement $prolongement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prolongement  $prolongement
     * @return \Illuminate\Http\Response
     */
    public function edit(Prolongement $prolongement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prolongement  $prolongement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prolongement $prolongement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prolongement  $prolongement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prolongement $prolongement)
    {
        //
    }
}
